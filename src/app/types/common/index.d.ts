import { TCellWritable } from "../../model/CellVO";

/**
 * created by mrzluka on 28.01.2022
 */

type TFunc<T = void> = () => T;


type TResponse = {
    error: string | undefined,
};

type THitResponse = {
    matrix: TCellWritable[][],
    isCrossPlayer: boolean,
} & TResponse;


type TCreateRoomResponse = {
    roomId: string;
} & TResponse;

type TGameResponse = {
    matrix: TCellWritable[][];
} & IRoomState & TResponse;

type TReqCreateRoom = {
    nickName: string;
}

type TEnterGameState = {nickName: string};


export type TGameStatus = "pending" | "started" | "paused" | "complete";

export interface IChatMessage {
    readonly msg: string;
    readonly nickName: string;
    readonly date: number;
    readonly msgType: "ownMessage" | "join" | "leave" | "";
}
export interface IRoomState {
    //matrix: number[][];
    players: string[];
    XPlayer: string;
    ZPlayer: string;
    roomId: string ;
    gameStatus: TGameStatus;
    winCombination: number[][];
    activePlayer: string;
    winner: string;
    chat: Readonly<IChatMessage[]>;
    get isGameStarted(): boolean;
    get isGameOver(): boolean;
}
