/**
 * created by mrzluka on 28.01.2022
 */
import { PlayFieldStore } from "./substore/PlayFieldStore";
import { action, computed, makeObservable, observable } from "mobx";
import { UserStore } from "./substore/UserStore";

export class RootStore {
    @computed
    get location(): { url: string; state: any } {
        return this._location;
    }

    constructor() {
        makeObservable(this);
    }

    @computed
    get userStore(): Readonly<UserStore> {
        return this._userStore;
    }

    @computed
    get playField(): Readonly<PlayFieldStore> {
        return this._playField;
    }

    @observable
    private _playField: PlayFieldStore = new PlayFieldStore();

    @observable
    private _userStore: UserStore = new UserStore();

    @observable.struct
    private _location: {
        url: string,
        state: any
    } = {
        url: "/",
        state: undefined
    }

    @action
    setLocation(url: string, state?: any) {
        this._location = {
            url,
            state
        }
    }
}
