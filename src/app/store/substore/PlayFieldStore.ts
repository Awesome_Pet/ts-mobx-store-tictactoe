/**
 * created by mrzluka on 28.01.2022
 */
import { action, computed, makeObservable, observable } from "mobx";
import { CellVO, TCellWritable } from "../../model/CellVO";
import { RoomVO } from "../../model/RoomVO";
import { IRoomState } from "../../types/common";

export class PlayFieldStore {
    @computed
    get fieldState(): CellVO[][] {
        return this._fieldState;
    }
    @computed
    get room(): IRoomState {
        return this._room;
    }

    @observable
    private _room: RoomVO = new RoomVO();
    @observable
    private _fieldState: CellVO[][] = [];

    constructor() {
        makeObservable(this);
        this.initField();
    }

    private initField() {
        this._fieldState = new Array(3).fill(0)
            .map(_ => new Array(3).fill(0)
                .map(_ => new CellVO()));
    }

    @action
    clear() {
        this._fieldState.forEach(row => {
            row.forEach(cell => {
                cell.clear();
            })
        });
        this._room.clear();
    }

    @action
    updateMatrix(matrix: TCellWritable[][]) {
        matrix.forEach((row, rowNdx) => {
            row.forEach((cell, cellNdx) => {
                if (this._fieldState[rowNdx][cellNdx].value !== cell) {
                    this._fieldState[rowNdx][cellNdx].setCellVal(cell);
                }
            })
        })
    }

    @action
    updateCell(val: TCellWritable, row: number, col: number) {
        this._fieldState[row][col].setCellVal(val);
    }

    @action
    updateCellWinning(val: boolean, row: number, col: number) {
        this._fieldState[row][col].setIsWin(val);
    }

    @action
    updateRoomState(room: IRoomState) {
        this._room.updateRoomState(room);
    }
}
