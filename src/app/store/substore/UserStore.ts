/**
 * created by mrzluka on 29.01.2022
 */
import { computed, makeObservable, observable } from "mobx";
import { IUser, UserVO } from "../../model/UserVO";
import { AccessProvider } from "../../provider/AccessProvider";

export class UserStore extends AccessProvider<{}> {

    @computed
    get user(): IUser {
        return this._user;
    }

    constructor() {
        super()
        makeObservable(this);
    }

    @observable
    private _user: UserVO = new UserVO();

    getUserVO(accessKey: symbol): UserVO  | never {
        this.hasAccess(accessKey, "You can't get access for UserVO")
        return this._user;
    }
}
