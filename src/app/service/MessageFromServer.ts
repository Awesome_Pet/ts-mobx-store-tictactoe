import { IMsgFromServer } from "./GameService";
import { TGameResponse } from "../types/common";
import { RootStore } from "../store/RootStore";
import { RoomVO } from "../model/RoomVO";

/**
 * created by mrzluka on 03.02.2022
 */

export class MessageFromServer {
    get connection(): IMsgFromServer {
        return this._connection;
    }
    private _store!: RootStore;

    private _connection: IMsgFromServer = {
        onStartNewGame: (data: TGameResponse) => {
            this.updateRoomState(data);
        },
        onJoin: (status: boolean) => {
            console.log("onJoin", status);
            if (!status) {
                this._store.setLocation("/");
            }
        },
        onMessage: (msg: string, nickName: string, date: number) => {
            const myNickName = this._store.userStore.user.nickName;
            const msgType = myNickName === nickName ? "ownMessage" : "";
            (this._store.playField.room as RoomVO).addNewMessage({
                msg,
                nickName,
                date,
                msgType,
            })
        },
        onGameAction: (data: TGameResponse) => {
            this.updateRoomState(data);
        },
        resumeGame: (data: TGameResponse) => {
            this.updateRoomState(data);
        },
        error: (msg: string) => {
            console.log("error", msg);
            //this._store.setLocation("/");
            alert(msg);
        },
        onUserJoined: (nickName: string, date: number) => {
            (this._store.playField.room as RoomVO).addNewMessage({
                msg: nickName + " joined",
                nickName: "",
                date,
                msgType: "join",
            })
        },
        onUserLeave: (nickName: string, date: number) => {
            (this._store.playField.room as RoomVO).addNewMessage({
                msg: nickName + " leave",
                nickName: "",
                date,
                msgType: "leave",
            })
        }
    }

    init(store: RootStore) {
        this._store = store;
    }

    private updateRoomState(data: TGameResponse) {
        if (this._store.playField.room.isGameOver && data.gameStatus === "started") {
            this._store.playField.clear();
        }
        this._store.playField.updateRoomState(data);
        this._store.playField.updateMatrix(data.matrix);
        if (data.winCombination && data.winCombination.length) {
            data.winCombination.forEach(cell => {
                this._store.playField.updateCellWinning(true, cell[0], cell[1]);
            })
        }
    }
}
