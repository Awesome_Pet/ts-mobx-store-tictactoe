/**
 * created by mrzluka on 28.01.2022
 */
import { RootStore } from "../store/RootStore";
import { AccessProvider } from "../provider/AccessProvider";
import { TCreateRoomResponse, TGameResponse, TReqCreateRoom, TResponse } from "../types/common";
import { io, Socket } from "socket.io-client";
import { MessageFromServer } from "./MessageFromServer";
import { isProduction } from "../utils";

type TKeys = {
    user?: any;
}

type TAction<T extends TResponse, B> = { type: string, respType?: T, body?: B };

const CREATE_ACTION: TAction<TCreateRoomResponse, TReqCreateRoom> = {type: "create"};

export interface IMsgFromServer {
    onJoin(status: boolean): void;

    onMessage(msg: string, nickName: string, date: number): void;

    error(msg: string): void;

    onGameAction(data: TGameResponse): void;

    onStartNewGame(data: TGameResponse): void;

    resumeGame(data: TGameResponse): void;

    onUserJoined(nickname: string, date: number): void;

    onUserLeave(nickname: string, date: number): void;
}

interface IMsgToServer {
    joinToRoom(roomId: string, userNick: string): void;

    hit(col: number, row: number): void;

    startNewGame(): void;

    sendMsg(msg: string): void;
}

export class GameService extends AccessProvider<TKeys> {
    readonly baseUrl = window.location.hostname;
    readonly baseSocketUrl = window.location.hostname;
    readonly apiPath = "api/";
    readonly apiPort = isProduction() ? window.location.port : 3001;
    readonly socketPort = isProduction() ? window.location.port : 3001;
    readonly socketProtocol = window.location.protocol.includes("https") ? "wss://" : "ws://";
    readonly apiProtocol = window.location.protocol;

    private _store!: RootStore;
    private _socket!: Socket<IMsgFromServer, IMsgToServer>;
    private _messageFromServer: MessageFromServer = new MessageFromServer();

    useStore(store: RootStore) {
        this._store = store;
        this.registerAccess(store.userStore, "user");
        this._messageFromServer.init(this._store);
    }

    createRoom(userNick: string, onComplete: (rooId: string) => void) {
        this.makePost(CREATE_ACTION, {
            nickName: userNick
        }, (resp) => {
            onComplete(resp.roomId);
        }, (err) => {
            alert(err);
        });
    }

    makeHit(col: number, row: number) {
        if (this._store.playField.room.isGameOver) {
            this._store.playField.clear();
            // noinspection TypeScriptValidateTypes
            this._socket.emit("startNewGame");
        } else {
            // noinspection TypeScriptValidateTypes
            this._socket.emit("hit", col, row);
        }
    }

    joinToRoom(roomId: string, userNick: string) {
        // noinspection TypeScriptValidateTypes
        this._socket.emit("joinToRoom", roomId, userNick);
        this._store.playField.clear();
        this._store.userStore.getUserVO(this._accessKeys.user).setUserData(userNick);
    }

    initServerListeners() {
        const conn = this._messageFromServer.connection;
        Object.keys(conn).forEach(messageFromServerKey => {
            this._socket.on(messageFromServerKey as any, (...args: unknown[]) => {
                ((conn as { [key: string]: any })[messageFromServerKey] as Function).apply(conn, args);
            })
        });
    }

    initSocketConnection() {
        this._socket = io(this.socketProtocol + this.baseSocketUrl + ":" + this.socketPort, {
            transports: ["websocket"]
        });

        this.initServerListeners();
    }

    protected makePost<T extends TResponse, B, R>(action: TAction<T, B>, data: B, onComplete: (data: T) => void, onError?: (err?: R) => void) {
        console.log(`${this.apiProtocol}//${this.baseUrl}:${this.apiPort}/${this.apiPath}${action.type}`);
        fetch(`${this.apiProtocol}//${this.baseUrl}:${this.apiPort}/${this.apiPath}${action.type}`, {
            method: "POST",
            headers: {
                "content-type": "text/plain" //prevent preflight request
            },
            body: JSON.stringify(data)
        }).then(res => {
            if (res.ok) {
                res.json().then((data: TResponse) => {
                    if (data.error) {
                        onError?.(data.error as unknown as R);
                    } else {
                        onComplete(data as unknown as T);
                    }
                })
            } else {
                onError?.();
            }
        }).catch(onError);
    }

    sendMessage(msg: string) {
        if (!this._socket || !this._socket.connected) return;
        // noinspection TypeScriptValidateTypes
        this._socket.emit("sendMsg", msg);
    }
}
