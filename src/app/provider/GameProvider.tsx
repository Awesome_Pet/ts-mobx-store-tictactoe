/**
 * created by mrzluka on 28.01.2022
 */
import { createContext, useContext } from "react";
import { GameService } from "../service/GameService";

const GameServiceContext = createContext({} as GameService);

export const GameServiceProvider = (props: {
    children: any,
    service: GameService
                                    }) => {
    return (<GameServiceContext.Provider value={props.service}>{props.children}</GameServiceContext.Provider>)
}

export const useGameService = () => useContext(GameServiceContext);
