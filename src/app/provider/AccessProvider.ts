/**
 * created by mrzluka on 29.01.2022
 */

export interface IAccessProvider {
    getAccessKey(): symbol;
}

export class AccessProvider<TKeys extends {[key: string]: any}> {
    protected _accessKeys: TKeys = {} as TKeys;
    private _isLocked = false;
    private readonly _accessKey = Symbol();

    registerAccess(provider: IAccessProvider, key: keyof TKeys) {
        this._accessKeys[key] = provider.getAccessKey() as any;
    }

    getAccessKey(): symbol {
        if (!this._isLocked) {
            return this._accessKey;
        }

        throw new Error("You can't get access in this place");
    }

    hasAccess(key: symbol, throwError = ""): boolean {
        if (key === this._accessKey) return true;
        if (throwError) throw new Error(throwError);
        return false;
    }

    lock() {
        this._isLocked = true;
    }

}
