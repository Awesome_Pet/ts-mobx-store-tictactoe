/**
 * created by mrzluka on 28.01.2022
 */
import { createContext, useContext } from "react";
import { RootStore } from "../store/RootStore";

const RootStoreContext = createContext({} as RootStore);

export const StoreProvider = (props: {
    children: any,
    value: RootStore
}) => {
    return (<RootStoreContext.Provider value={props.value}>{props.children}</RootStoreContext.Provider>)
}

export const useStore = () => useContext(RootStoreContext);
