import styled from "styled-components";
import { baseAnim } from "./Cell.style";

/**
 * created by mrzluka on 28.01.2022
 */


export const XVal = styled(baseAnim)`
  font-size: 1em;
  font-family: monospace;
  user-select: none;
  &.text-in {
    -webkit-animation: text-in 300ms cubic-bezier(0.550, 0.085, 0.680, 0.530) both;
    animation: text-in 300ms cubic-bezier(0.550, 0.085, 0.680, 0.530) both;
  }
`;
export const ZVal = styled(baseAnim)`
  margin-top: .37em;
  font-size: 1.1em;
  font-family: monospace;
  user-select: none;
  &.text-in {
    -webkit-animation: text-in 300ms cubic-bezier(0.550, 0.085, 0.680, 0.530) both;
    animation: text-in 300ms cubic-bezier(0.550, 0.085, 0.680, 0.530) both;
  }
`;
