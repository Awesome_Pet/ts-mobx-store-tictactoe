/**
 * created by mrzluka on 28.01.2022
 */
import { XVal, ZVal } from "./CellValue.style";


export const CellValue = (props: {
    isCross: boolean
}) => {
    return props.isCross ? <XVal className={"text-in"}>×</XVal> : <ZVal className={"text-in"}>°</ZVal>;
}
