/**
 * created by mrzluka on 28.01.2022
 */
import { Block } from "./Cell.style";
import { useEffect, useState } from "react";

export const Cell = (props: {
    isEnabled?: boolean;
    isWin?: boolean;
    isLose?: boolean;
    children?: any;
    onClick: (col: number, row: number) => void;
    row: number;
    col: number
}) => {

    const onCellClick = (_: any) => {
        if (props.isEnabled) {
            props.onClick(props.col, props.row);
        }
    }

    const [classList, setClassList] = useState([] as string[]);
    useEffect(() => {
        let c = [];
        !props.isEnabled && c.push("done");
        props.isWin && c.push("win");
        props.isLose && c.push("lose");
        setClassList(c);
    }, [props.isEnabled, props.isWin, props.isLose]);

    return (
        <Block className={classList.join(" ")} onClick={onCellClick}>{props.children}</Block>
    );
}
