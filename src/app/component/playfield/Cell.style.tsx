import styled from "styled-components";

/**
 * created by mrzluka on 28.01.2022
 */


export const Block = styled.div`
  width: calc(100% / 3);
  height: 100%;
  background-color: #d3d3d3;
  color: black;
  text-align: center;
  border: 2px solid black;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    background-color: white;
    cursor: pointer;

    &.done {
      background-color: #d3d3d3;
      cursor: auto;
    }

    &.win {
      background-color: #017911;
    }

    &.lose {
      background-color: red;
    }
  }

  &.win {
    background-color: #017911;
  }

  &.lose {
    background-color: red;
  }
`;



export const baseAnim = styled.div`
  @-webkit-keyframes text-in {
    0% {
      -webkit-filter: blur(12px);
      filter: blur(12px);
      opacity: 0;
    }
    100% {
      -webkit-filter: blur(0px);
      filter: blur(0px);
      opacity: 1;
    }
  }
  @keyframes text-in {
    0% {
      -webkit-filter: blur(12px);
      filter: blur(12px);
      opacity: 0;
    }
    100% {
      -webkit-filter: blur(0px);
      filter: blur(0px);
      opacity: 1;
    }
  }

`;
