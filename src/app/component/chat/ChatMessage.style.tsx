import styled from "styled-components";

export const ChatMessageContainer = styled.div`  
  margin: 0.4rem;
  text-align: left;
  font-family: monospace;
`;

export const ChatMessageBg = styled.div`
  background-color: #127df7;
  float: left;
  border-radius: 0.5rem;
  padding: 0.5rem;
  padding-top: 0;
  min-width: 5rem;
  min-height: 3rem;
  max-width: 80%;
  &.ownMessage {
    text-align: right;
    float: right;
  }
  &.leave {
    text-align: center;
    float: none;
    margin: auto;
    min-height: auto;
    background-color: red;
  }
  &.join {
    text-align: center;
    float: none;
    margin: auto;
    min-height: auto;
    background-color: green;
  }
`;

export const NickHeader = styled.div`
  color: yellow;
`;

export const MessageSpan = styled.span`
    font-size: 1rem;
`;
