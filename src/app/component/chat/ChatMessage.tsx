import { ChatMessageBg, ChatMessageContainer, MessageSpan, NickHeader } from "./ChatMessage.style";

export const ChatMessage = ({msg, msgType, nick}: {
    msg: string,
    nick: string,
    msgType: "ownMessage" | "join" | "leave" | "";
}) => {

    return (
        <ChatMessageContainer className={msgType}>
            <ChatMessageBg className={msgType}>
                <NickHeader>{nick}</NickHeader>
                <MessageSpan>{msg}</MessageSpan>
            </ChatMessageBg>

        </ChatMessageContainer>
    )
};
