import { Button } from "./ButtonBase.style";
import { TFunc } from "../../types/common";

export type pButtonBase = {
    onClick: TFunc;
    children: any;
    type: "createGame" | "normal" | "send";
};

export const ButtonBase = (props: pButtonBase) => {
    return (
        <Button className={props.type} onClick={props.onClick}>{props.children}</Button>
    );
}
