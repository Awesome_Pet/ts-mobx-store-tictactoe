import styled from "styled-components";

export const Button = styled.button`
  font-size: 1rem;
  border-radius: 5px;
  border: none;
  padding: 10px;
  font-family: monospace, serif;
  user-select: none;
  &.createGame {
    background-color: #006dbb;
    color: white;

    &:hover {
      background-color: #0081da;
      cursor: pointer;
    }

    &:active {
      background-color: #0098ff;
    }
  }

  &.send {
    background-color: #ff5959;
    font-size: .8rem;
    border-radius: 3px;
    padding: 0.2rem;
    color: white;
    &:hover {
      background-color: #fe8585;
      cursor: pointer;
    }

    &:active {
      background-color: #fc2d2d;
    }
  }
`;
