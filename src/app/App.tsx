import React from "react";
import { ThemeProvider } from "styled-components";
import { DarkTheme } from "./theme/dark/Dark.theme";
import { AppContainer } from "./App.style";
import { GameServiceProvider } from "./provider/GameProvider";
import { GameService } from "./service/GameService";
import { StoreProvider } from "./provider/StoreProvider";
import { RootStore } from "./store/RootStore";
import { Home } from "./page/Home";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Game } from "./page/Game";
import { CustomRouter } from "./router/CustomRouter";
import { isProduction } from "./utils";

const store = new RootStore();
const gameService = new GameService();

gameService.useStore(store);
//gameService.registerAccess(store.userStore, 'user');
store.userStore.lock();

function App() {
    return (
        <ThemeProvider theme={DarkTheme}>
            <AppContainer>
                <StoreProvider value={store}>
                    <GameServiceProvider service={gameService}>
                        <BrowserRouter basename={isProduction() ? "/tictac" : "/"}>
                            <CustomRouter/>
                            <Routes >
                                <Route path="/" element={<Home/>}/>
                                <Route path="/room/:id" element={<Game/>}/>
                            </Routes>
                        </BrowserRouter>
                    </GameServiceProvider>
                </StoreProvider>
            </AppContainer>
        </ThemeProvider>
    );
}

export default App;
