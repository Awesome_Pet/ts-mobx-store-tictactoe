export const DarkTheme = {
    color: "#eee",
    backgroundColor: "#444",
    buttons: {
        base: {
            color: "#eee",
            backgroundColor: "#1a58a9",
        }
    }
}
