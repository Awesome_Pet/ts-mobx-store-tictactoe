/**
 * created by mrzluka on 09.02.2022
 */

export function isProduction() {
    return process.env.NODE_ENV === "production";
}
