/**
 * created by mrzluka on 30.01.2022
 */

type TValidationResult<T> = {
    valid: boolean,
    validationResult: T;
    error?: string,
}


export function isUserNameValid(val: string | undefined): TValidationResult<string> {
    let res: TValidationResult<string> = {valid: false, validationResult: ''};

    if (!val) return res;

    val = val.trim();
    if (!val || val.length < 3) {
        res.error = "Too short (min 3 chars)";
        return res;
    }

    if (val.length > 8) {
        res.error = "Too long (max 8 chars)";
        return res;
    }
    res.valid = true;
    res.validationResult = val;
    return res;
}
