import { useLocation, useNavigate } from "react-router-dom";
import { useStore } from "../provider/StoreProvider";
import { useEffect } from "react";
import { observer } from "mobx-react";

export const CustomRouter = observer(() => {
    const navigate = useNavigate();
    const location = useLocation();
    const store = useStore();

    useEffect(() => {
        console.log("set default location", location.pathname, location.state);
        store.setLocation(location.pathname, location.state);
    }, []);

    useEffect(() => {
        console.log("navigate to ", store.location.url, store.location.state);
        navigate(store.location.url, {
            state: store.location.state
        });
    }, [store.location]);

    return <></>
});
