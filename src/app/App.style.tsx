import styled from "styled-components";

export const AppContainer = styled.div`
  background-color: ${props => props.theme.backgroundColor};
  color: ${props => props.theme.color};
  height: 100%;
  position: absolute;
  width: 100%;

  display: flex;
  align-items: center;
  justify-content: center;
`;

