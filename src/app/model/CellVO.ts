/**
 * created by mrzluka on 28.01.2022
 */
import { action, computed, makeObservable, observable } from "mobx";

export type TCellVal = -1 | 1 | 0;
export type TCellWritable = Exclude<TCellVal, -1>

export class CellVO {
    constructor() {
        makeObservable(this);
    }

    @computed
    get value(): TCellVal {
        return this._value;
    }
    @computed
    get isWinCell(): boolean {
        return this._isWinCell;
    }

    @observable
    private _value: TCellVal = -1;
    @observable
    private _isWinCell: boolean = false;

    @action
    setCellVal(val: TCellWritable) {
        this._value = val;
    }
    @action
    setIsWin(val: boolean) {
        this._isWinCell = val;
    }

    @action
    clear() {
        this._value = -1;
        this._isWinCell = false;
    }
}
