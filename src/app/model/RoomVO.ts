/**
 * created by mrzluka on 31.01.2022
 */
import { action, computed, makeObservable, observable } from "mobx";
import { IChatMessage, IRoomState, TGameStatus } from "../types/common";

export class RoomVO implements IRoomState {
    @computed get chat(): Readonly<IChatMessage[]> {
        return this._chat;
    }
    @computed get roomId(): string {
        return this._roomId;
    }
    @computed get winCombination(): number[][] {
        return this._winCombination;
    }
    @computed get players(): string[] {
        return this._players;
    }
    @computed get activePlayer(): string {
        return this._activePlayer;
    }
    @computed get gameStatus(): TGameStatus {
        return this._gameStatus;
    }
    @computed get winner(): string {
        return this._winner;
    }
    @computed get ZPlayer(): string {
        return this._ZPlayer;
    }
    @computed get XPlayer(): string {
        return this._XPlayer;
    }

    constructor() {
        makeObservable(this)
    }

    @action
    updateRoomState(room: IRoomState) {
        this._activePlayer = room.activePlayer;
        this._XPlayer = room.XPlayer;
        this._ZPlayer = room.ZPlayer;
        this._winCombination = room.winCombination;
        this._winner = room.winner;
        this._gameStatus = room.gameStatus;
        this._players = room.players;
    }

    @action
    addNewMessage(msg: IChatMessage) {
        this._chat.unshift(msg);
    }

    @computed
    get isGameStarted() {
        return this._gameStatus !== "pending";
    }
    @computed
    get isGameOver() {
        return this._gameStatus === "complete";
    }

    @action
    clear() {
        //this._matrix = [];
        this._activePlayer = "";
        this._XPlayer = "";
        this._ZPlayer = "";
        this._winCombination = [];
        this._winner = "";
        this._gameStatus = "pending";
    }

    @observable private _XPlayer: string = "";
    @observable private _ZPlayer: string = "";
    @observable private _activePlayer: string = "";
   // @observable private _matrix: number[][] = [];
    @observable private _players: string[] = [];
    @observable private _roomId: string = "";
    @observable private _winCombination: number[][] = [];
    @observable private _winner: string = "";
    @observable private _gameStatus: TGameStatus = "pending";
    @observable private _chat: IChatMessage[] = [];

}
