import { action, computed, makeObservable, observable } from "mobx";

/**
 * created by mrzluka on 29.01.2022
 */

export interface IUser {
    readonly nickName: string;
    readonly isXPLayer: boolean;
    readonly userId: string;
}

export class UserVO implements IUser {
    @computed
    get nickName(): string {
        return this._nickName;
    }
    @computed
    get isXPLayer(): boolean {
        return this._isXPLayer;
    }

    @computed
    get userId(): string {
        return this._userId;
    }

    constructor() {
        makeObservable(this);
    }

    @action
    setUserData(nick: string, playerId: string = "") {
        this._userId = playerId;
        this._nickName = nick;
    }

    @action
    setXPlayer(isXPlayer: boolean) {
        this._isXPLayer = isXPlayer;
    }

    @observable
    private _userId: string = "";

    @observable
    private _nickName: string = "";

    @observable
    private _isXPLayer: boolean = false;

}
