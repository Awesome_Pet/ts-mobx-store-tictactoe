import { HeaderText, NewGameContainer } from "./Home.style";
import { useCallback, useState } from "react";
import { useGameService } from "../provider/GameProvider";
import { TEnterGameState } from "../types/common";
import { NickName } from "../compossables/input/NickName";
import { FormContainer } from "../style";
import { useStore } from "../provider/StoreProvider";

export const Home = () => {
    const [isReady, setIsReady] = useState(false);
    const gameService = useGameService();
    const store = useStore();
    const onClick = useCallback((nickName: string) => {
        setIsReady(true);
        gameService.createRoom(nickName, (roomLink) => {
            store.setLocation("/room/" + roomLink, {
                nickName
            } as TEnterGameState);
        });
    }, [gameService]);
    return (
        <FormContainer>
            <HeaderText className={isReady ? "ready" : ""}>Tic tac toe game</HeaderText>
            <NewGameContainer className={isReady ? "ready" : ""}>
                <p>To start, press 'Create game'</p>
                <NickName btnText={"Create game"} onSuccess={onClick}/>
            </NewGameContainer>
        </FormContainer>
    );
}
