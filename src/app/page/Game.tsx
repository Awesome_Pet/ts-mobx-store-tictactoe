import { PlayField } from "../compossables/playfield/PlayField";
import { useLocation } from "react-router-dom";
import { TEnterGameState } from "../types/common";
import { NickName } from "../compossables/input/NickName";
import { FormContainer } from "../style";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useGameService } from "../provider/GameProvider";
import { PlayersInfo } from "../compossables/playfield/PlayersInfo";
import { ChatGameHolder, GameContainer } from "./Game.style";
import { Chat } from "../compossables/chat/Chat";

export const Game = () => {
    const location = useLocation();
    const gameService = useGameService();

    const state = location.state as TEnterGameState;
    const [nickName, setNickName] = useState(state && state.nickName);

    const roomId = useMemo(() => {
        const roomMatch = location.pathname.match(/room\/\d+/);
        if (roomMatch && roomMatch.length) {
            return roomMatch[0].split("room/")[1];
        }
        return "";
    }, [location.pathname]);

    useEffect(() => {
        console.log("Do join");
        if (nickName) {
            console.log("Do join", nickName);
            gameService.initSocketConnection();
            gameService.joinToRoom(roomId, nickName);
        }
    }, [nickName]);

    const onEnterNickName = useCallback((nickName: string) => {
        setNickName(nickName);
    }, []);

    return (
        !!nickName ? (
            <GameContainer>
                <div>
                    <PlayersInfo/>
                    <PlayField/>
                </div>
                <ChatGameHolder>
                    <Chat/>
                </ChatGameHolder>
            </GameContainer>

        ) : (
            <FormContainer>
                <p>To start the game, enter your nick name</p>
                <NickName onSuccess={onEnterNickName} btnText={"Join"}/>
            </FormContainer>
        )
    );
}
