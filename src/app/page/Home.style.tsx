import styled from "styled-components";

export const HeaderText = styled.h2`
  font-size: 1.5rem;
  transition: margin-top 500ms ease-out;
  &.ready {
    margin-top: 7rem;
  }
`;

export const NewGameContainer = styled.div`
    &.ready {
      visibility: hidden;
    }
`;
