import { ChatInputField } from "./ChatInput.style";
import React, { ForwardedRef, forwardRef, useImperativeHandle, useRef } from "react";
import { TFunc } from "../../types/common";

export type TChatInputActions = {
    clear: TFunc
    setFocus: TFunc
}
export const ChatInput = forwardRef(({onChangeCb, onSubmit}: {
    onChangeCb: (msg: string) => void
    onSubmit: TFunc
}, ref: ForwardedRef<TChatInputActions>) => {
    const inputField = useRef<HTMLTextAreaElement>({} as HTMLTextAreaElement);
    const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        onChangeCb(e.target.value.trim());
    }

    const onKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
        if (e.key === "Enter") {
            if (!e.shiftKey) {
                e.preventDefault();
                onSubmit();
            }
        }
    }

    useImperativeHandle(ref, () => ({
        clear() {
            inputField.current.value = "";
            onChangeCb("");
        },
        setFocus() {
            inputField.current.focus();
        }
    }));

    return (
        <ChatInputField ref={inputField} onChange={onChange} onKeyDown={onKeyDown}/>
    )
});
