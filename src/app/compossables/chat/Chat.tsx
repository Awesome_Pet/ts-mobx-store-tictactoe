import { ChatContainer, ChatControlContainer, ChatInputContainer, MessagesContainer } from "./Chat.style";
import { ChatMessage } from "../../component/chat/ChatMessage";
import { ChatInput, TChatInputActions } from "./ChatInput";
import { ButtonBase } from "../../component/button/ButtonBase";
import { useStore } from "../../provider/StoreProvider";
import { useCallback, useRef, useState } from "react";
import { useGameService } from "../../provider/GameProvider";
import { observer } from "mobx-react";

export const Chat = observer(() => {
    const store = useStore();
    const service = useGameService();
    const chat = store.playField.room.chat;
    const chatRef = useRef<TChatInputActions>({} as TChatInputActions);
    const [message, setMessage] = useState("");

    const onSendClick = useCallback(() => {
        if (message.length) {
            service.sendMessage(message);
            chatRef.current.clear();
            chatRef.current.setFocus();
        }

    }, [chatRef, service, message]);

    const onChange = useCallback((msg) => {
        setMessage(msg);
    }, [setMessage]);

    return (
        <ChatContainer>
            <MessagesContainer>
                {chat.map(msg => <ChatMessage msg={msg.msg} msgType={msg.msgType} nick={msg.nickName}
                                              key={msg.date}/>)}
            </MessagesContainer>
            <ChatInputContainer>
                <ChatInput ref={chatRef} onChangeCb={onChange} onSubmit={onSendClick}/>
            </ChatInputContainer>
            <ChatControlContainer>
                <ButtonBase type={"send"} onClick={onSendClick}>Send</ButtonBase>
            </ChatControlContainer>
        </ChatContainer>
    )
});
