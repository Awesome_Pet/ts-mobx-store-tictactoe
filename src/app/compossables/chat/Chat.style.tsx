/**
 * created by mrzluka on 01.02.2022
 */
import styled from "styled-components";

export const ChatContainer = styled.div`
  background-color: darkslategray;
  height: 25rem;
  width: 20rem;
  border-radius: 10px;
  
`;

export const MessagesContainer = styled.div`
  height: 70%;
  overflow-y: scroll;
  display: flex;
  flex-direction: column-reverse;
`;


export const ChatInputContainer = styled.div`
  height: 23%;
  text-align: center;
`;

export const ChatControlContainer = styled.div`
  height: 5%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: calc(100% - 0.5rem);
`;
