import styled from "styled-components";

export const ChatInputField = styled.textarea`
  resize: none;
  outline: none;
  width: calc(95% - 0.4rem);
  height: calc(100% - 1.2rem);
  text-align: right;
  border: 0;
  padding: 0.2rem;
  margin-top: 0.4rem;
  color: white;
  background-color: #1f3434;
`;
