import {
    CellValContainer,
    GameHint,
    PlayerContainer,
    PlayersContent,
    PlayersInfoContainer,
    VSContainer
} from "./PlayersInfo.style";
import { CellValue } from "../../component/playfield/CellValue";
import { useStore } from "../../provider/StoreProvider";
import { observer } from "mobx-react";

export const PlayersInfo = observer(() => {
    const store = useStore();
    const room = store.playField.room;
    const userNick = store.userStore.user.nickName;
    const xPlayer = room.XPlayer;
    const zPlayer = room.ZPlayer;
    const isGameStarted = room.isGameStarted;
    const mate = userNick === xPlayer ? zPlayer : xPlayer;
    const activePlayer = room.activePlayer;

    const getHintText = () => {
        if (room.gameStatus === "pending" || room.gameStatus === "paused") {
            return "Wait yor mate";
        }

        if (room.winner) {
            if (room.winner === userNick) {
                return "Wow you won!";
            } else {
                return "Better next time :)";
            }
        }

        if (room.isGameOver) {
            return "Draw";
        }

        if (activePlayer === userNick) {
            return "You turn"
        } else {
            return "opponent's turn"
        }
    };
    return (
        <PlayersInfoContainer>
            <h3>Tic Tac Toe</h3>
            <PlayersContent className={isGameStarted ? "gameStarted" : ""}>
                <PlayerContainer className={activePlayer === userNick ? "active" : ""}>
                    <div>{userNick}</div>
                    <CellValContainer>
                        <CellValue isCross={userNick === xPlayer}/>
                    </CellValContainer>
                </PlayerContainer>
                <VSContainer>VS</VSContainer>
                <PlayerContainer className={activePlayer !== userNick ? "active" : ""}>
                    <div>{mate}</div>
                    <CellValContainer>
                        <CellValue isCross={userNick !== xPlayer}/>
                    </CellValContainer>
                </PlayerContainer>
            </PlayersContent>
            <GameHint>{getHintText()}</GameHint>
        </PlayersInfoContainer>
    )
});
