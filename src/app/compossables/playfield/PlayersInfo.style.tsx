import styled from "styled-components";

export const PlayersInfoContainer = styled.div`
  text-align: center;
  font-family: monospace;
`;

export const PlayersContent = styled.div`
  display: flex;
  justify-content: space-between;
  visibility: hidden;
  &.gameStarted {
    visibility: visible;
  }
`;

export const CellValContainer = styled.div`
  font-size: 10rem;
  height: 10rem;
  margin-top: -2rem;
  margin-bottom: -3rem;
  display: flex;
  justify-items: center;
  align-items: center;
`;

export const PlayerContainer = styled.div`
  font-size: 1.2rem;
    &.active {
      color: yellow;
    }
`;

export const VSContainer = styled.div`
  font-size: 2rem;
  margin-top: 2.5rem;
`;

export const GameHint = styled.p`
  font-size: 1rem;
`;
