import { PlayFieldContainer, PlayRow } from "./PlayField.style";
import { Cell } from "../../component/playfield/Cell";
import { CellValue } from "../../component/playfield/CellValue";
import { useCallback, useMemo } from "react";
import { useStore } from "../../provider/StoreProvider";
import { observer } from "mobx-react";
import { useGameService } from "../../provider/GameProvider";

export const PlayField = observer(() => {
    const store = useStore();
    const service = useGameService();


    const onCellClick = useCallback((col: number, row: number) => {
        service.makeHit(col, row);
    }, [service]);


    const state = store.playField.fieldState;
    const room = store.playField.room;
    const isEnabled = useMemo(() => {
        return room.activePlayer === store.userStore.user.nickName;
    }, [room.activePlayer, store.userStore.user.nickName]);
    const isPlayerLoose = room.winner !== store.userStore.user.nickName;
    return (
        <PlayFieldContainer>
            {state.map((playRow, rowNdx) => (
                <PlayRow key={rowNdx}>
                    {state[rowNdx].map((cellVal, colNdx) => (
                        <Cell isEnabled={room.isGameOver || (cellVal.value === -1 && isEnabled)}
                              isWin={cellVal.isWinCell && !isPlayerLoose}
                              isLose={cellVal.isWinCell && isPlayerLoose}
                              onClick={onCellClick} col={colNdx} row={rowNdx}
                              key={`${rowNdx}${colNdx}`}> {
                            cellVal.value !== -1 ? <CellValue isCross={cellVal.value === 1}/> : undefined
                        }
                        </Cell>
                    ))}
                </PlayRow>
            ))}
        </PlayFieldContainer>
    )
});
