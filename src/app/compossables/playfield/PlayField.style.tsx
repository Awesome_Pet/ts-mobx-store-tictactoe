import styled from "styled-components";

export const PlayRow = styled.div`
  display: flex;
  justify-content: middle;
  position: relative;
  width: 100%;
  height: calc(100% / 3);
  font-size: 12rem;
`;

export const PlayFieldContainer = styled.div`
  width: 20rem;
  height: 20rem;
`;


