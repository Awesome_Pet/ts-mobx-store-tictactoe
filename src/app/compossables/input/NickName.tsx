import { ButtonBase } from "../../component/button/ButtonBase";
import { useRef } from "react";
import { isUserNameValid } from "../../validator";
import { NickInput, NickNameContainer } from "./NickName.style";

/**
 * created by mrzluka on 30.01.2022
 */

export function NickName(props: {
    onSuccess: (nickName: string) => void,
    btnText: string,
}) {
    const input = useRef<HTMLInputElement | null>(null);
    const onClick = () => {
        const vResult = isUserNameValid(input?.current?.value);
        if (vResult.valid) {
            props.onSuccess(vResult.validationResult);
        } else {
            alert(vResult.error);
        }
    };
    return (
        <NickNameContainer>
            <div>
                <label htmlFor={"nickName"}>Nickname</label>
                <br/>
                <NickInput ref={input} type={"text"} name={"nickName"}/>
            </div>
            <ButtonBase onClick={onClick} type={"createGame"}>{props.btnText}</ButtonBase>
        </NickNameContainer>
    );
}
