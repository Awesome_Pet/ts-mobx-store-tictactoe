import styled from "styled-components";

export const NickNameContainer = styled.div`
  text-align: center;
`;
export const NickInput = styled.input`
  margin: .5rem 0;
`;
