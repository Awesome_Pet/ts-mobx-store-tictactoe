FROM node:lts-alpine

WORKDIR /var/www/server

COPY ./server/package*.json ./
RUN npm ci

WORKDIR /var/www

COPY ./packa*.json ./
RUN npm ci

WORKDIR /var/www/server
COPY ./server/tsconfig.json ./
COPY ./server/*.ts ./
COPY ./server/model ./model/
COPY ./server/src ./src/
RUN npm run build

WORKDIR /var/www
COPY ./public ./public/
COPY ./tsconfig.json ./
COPY ./src ./src/

RUN export NODE_ENV=production
RUN npm run build

CMD ["node", "./server/dist/index.js"]


