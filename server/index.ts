import { TCreateRoomResponse, TResponse } from "./model/Types";
import { Socket } from "socket.io-client";
import { Room } from "./src/Room";
const path = require("path");

const express = require("express");
const app = express();
const http = new (require("http")).Server(app);
const io = require("socket.io")(http);

const bodyParser = require("body-parser");
const cors = require("cors");

app.use("/", express.static("build"));

app.use(cors({
    origin: "*"
}));

app.use(bodyParser.json())

const __playersMap: Map<Socket, string> = new Map<Socket, string>();
const __roomsMap: Map<string, Room> = new Map<string, Room>();

io.on("connection", (socket: Socket) => {
    console.log("A user connected", socket.id);
    socket.on("disconnect", () => {
        console.log("A user disconnected");
        const roomId = __playersMap.get(socket);
        const room = __roomsMap.get(roomId);
        if (room) {
            room.ouUserLeave(socket);
        }
        __playersMap.delete(socket);
    });

    socket.on("joinToRoom", (roomId, userNick) => {
        console.log("joinToRoom", roomId);
        const room = __roomsMap.get(roomId)
        if (!room) {
            socket.emit("onJoin", false);
            return;
        } else {
            if (room.ouUserJoin(userNick, socket)) {
                __playersMap.set(socket, roomId);
                socket.emit("onJoin", true);
                if (room.canGameStart()) {
                    room.startGame();
                } else if (room.canGameResume()) {
                    room.resumeGame();
                }
            } else {
                socket.emit("onJoin", false);
            }
        }
    });

    socket.on("hit", (col, row) => {
        const roomId = __playersMap.get(socket);
        const room = __roomsMap.get(roomId);
        if (!room) {
            socket.emit("error", "Room not found!");
            return;
        }
        const result = room.playerHit(socket, col, row);
        if (typeof result === "string") {
            socket.emit("error", result);
        }
    });

    socket.on("startNewGame", () => {
        const roomId = __playersMap.get(socket);
        const room = __roomsMap.get(roomId);
        if (!room) {
            socket.emit("error", "Room not found!");
            return;
        }
        room.resetGame();
        if (room.canGameStart()) {
            room.startGame();
        }
    });

    socket.on("sendMsg", (msg: string) => {
        const roomId = __playersMap.get(socket);
        const room = __roomsMap.get(roomId);
        if (!room) {
            socket.emit("error", "Room not found!");
            return;
        }
        room.sendMessage(msg, socket);

    });
});

function createNewRoom(): string {
    const roomId = Date.now() + "";
    __roomsMap.set(roomId, new Room(roomId));
    return roomId;
}

function prepareCreateRoomResponse(): TResponse {
    const room = createNewRoom();
    return {
        roomId: room
    } as TCreateRoomResponse
}

app.post("/api/create", function (req, res, next) {
    res.send(JSON.stringify(
        prepareCreateRoomResponse()
    ));
    res.end();
});


app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname, '../../build/index.html'));
});

http.listen(3001,  function () {
    console.log("listening on *:3001");
});
