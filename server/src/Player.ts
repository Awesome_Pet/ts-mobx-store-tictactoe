/**
 * created by mrzluka on 31.01.2022
 */
import { Socket } from "socket.io-client";

export class Player {

    constructor(public nickName: string, public socket: Socket) {
    }
}
