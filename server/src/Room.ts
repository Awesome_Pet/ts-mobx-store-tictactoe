import { Player } from "./Player";
import { Socket } from "socket.io-client";
import { EError, TRoom } from "../model/Types";

/**
 * created by mrzluka on 31.01.2022
 */

enum ERoomActions {
    JOIN_TO_ROOM = "joinToRoom",
    START_NEW_GAME = "onStartNewGame",
    ON_ACTION = "onGameAction",
    RESUME_GAME = "resumeGame",
    ON_PLAYER_LEAVE = "onLeave",
    SEND_MESSAGE = "onMessage",
    onUserJoined = "onUserJoined",
    onUserLeave = "onUserLeave",
}

export class Room {
    players: Player[] = [];
    matrix: number[][];
    XPlayer: Player | undefined;
    ZPlayer: Player | undefined;
    activePlayer: Player | undefined;
    winner: Player | undefined;
    winCombination: number[][]
    gameStatus: "pending" | "started" | "paused" | "complete" = "pending";

    constructor(public roomId: string) {
    }

    getDataToUpdate(): object {
        return {
            matrix: this.matrix,
            players: this.players.map(player => player.nickName),
            XPlayer: this.XPlayer?.nickName,
            ZPlayer: this.ZPlayer?.nickName,
            roomId: this.roomId,
            winCombination: this.winCombination,
            activePlayer: this.activePlayer?.nickName,
            gameStatus: this.gameStatus,
            winner: this.winner?.nickName,
        }
    }

    protected sendRoomUpdate(action: string) {
        this.players.forEach(player => {
                player.socket.emit(action, this.getDataToUpdate());
            }
        );
    }

    ouUserLeave(socket: Socket) {
        const ndx = this.players.findIndex(player => player.socket === socket);
        if (ndx > -1) {
            const player = this.players.splice(ndx, 1)[0];
            if (this.ZPlayer === player) this.ZPlayer = undefined;
            if (this.XPlayer === player) this.XPlayer = undefined;
            if (this.activePlayer === player) this.activePlayer = undefined;

            if(this.gameStatus === "started") {
                this.gameStatus = "paused";
            }
            this.sendRoomUpdate(ERoomActions.ON_PLAYER_LEAVE);
            this.players.forEach(p => p.socket.emit(ERoomActions.onUserLeave, player.nickName, Date.now().toString()));

        }
    }

    ouUserJoin(nickName: string, socket: Socket): boolean {
        if (this.players.length < 2) {
            this.players.push(new Player(nickName, socket))
            this.players.forEach(player => player.socket.emit(ERoomActions.onUserJoined, nickName, Date.now().toString()));
            return true;
        }
        return false;
    }


    canGameStart(): boolean {
        return (this.gameStatus === "pending" || this.gameStatus === "complete") && this.players.length === 2;
    }

    canGameResume(): boolean {
        return (this.gameStatus === "paused") && this.players.length === 2;
    }

    resetGame() {
        this.gameStatus = "pending";
    }

    startGame() {
        this.gameStatus = "started";
        this.matrix = this.getNewMatrix();
        const XPlayerIndex = Math.round(Math.random())
        const ZPlayerIndex = XPlayerIndex ? 0 : 1;
        this.XPlayer = this.players[XPlayerIndex];
        this.ZPlayer = this.players[ZPlayerIndex];
        this.activePlayer = this.XPlayer;
        this.winner = undefined;
        this.winCombination = undefined;

        this.sendRoomUpdate(ERoomActions.START_NEW_GAME);
    }

    resumeGame() {
        this.gameStatus = "started";
        const oldPlayer = this.XPlayer || this.ZPlayer;
        const newPlayer = this.players.find(player => player !== oldPlayer);
        if (!this.ZPlayer) this.ZPlayer = newPlayer;
        if (!this.XPlayer) this.XPlayer = newPlayer;
        if (!this.activePlayer) this.activePlayer = newPlayer;

        this.sendRoomUpdate(ERoomActions.RESUME_GAME);
    }

    playerHit(socket: Socket, col: number, row: number): TRoom | EError {
        if (this.winner) return EError.GAME_OVER;
        const currentPlayer = this.players.find(player => player.socket === socket);
        if (!currentPlayer) return EError.WRONG_PLAYER;
        if (this.activePlayer !== currentPlayer) return EError.WRONG_ACTION;
        if (this.players.length !== 2) return EError.WAIT_SECOND_PLAYER;
        if (row > 2 || row < 0 || col > 2 || col < 0) return EError.WRONG_INDEX;
        if (this.matrix[row][col] !== -1) return EError.CELL_ALL_READY_CLICKED;

        this.matrix[row][col] = this.XPlayer === currentPlayer ? 1 : 0;

        this.activePlayer = currentPlayer === this.XPlayer ? this.ZPlayer : this.XPlayer;
        this.checkForWinning(currentPlayer);
        this.sendRoomUpdate(ERoomActions.ON_ACTION);
    }

    protected checkForWinning(player: Player) {
        const m = this.matrix;
        const searchRez = this.XPlayer === player ? 1 : 0;
        let winCombination: number[][];
        const winCombinations = [
            [[0, 0], [0, 1], [0, 2]],
            [[1, 0], [1, 1], [1, 2]],
            [[2, 0], [2, 1], [2, 2]],

            [[0, 0], [1, 0], [2, 0]],
            [[0, 1], [1, 1], [2, 1]],
            [[0, 2], [1, 2], [2, 2]],

            [[0, 0], [1, 1], [2, 2]],
            [[0, 2], [1, 1], [2, 0]],
        ];
        winCombinations.forEach(combination => {
            if (combination.filter(cell => m[cell[0]][cell[1]] === searchRez).length === 3) {
                winCombination = combination;
            }
        });

        if (winCombination) {
            this.winner = this.players.find(p => p === player);
            this.winCombination = winCombination;
            this.activePlayer = undefined;
            this.gameStatus = "complete";
        } else {
            const hasActions = [...m[0], ...m[1], ...m[2]].find(el => el === -1);
            if (!hasActions) {
                this.gameStatus = "complete";
            }
        }
    }


    protected getNewMatrix(): number[][] {
        return new Array(3).fill(0).map(_ => new Array(3).fill(0).map(_ => -1));
    }


    sendMessage(msg: string, socket: Socket) {
        const nickName = this.players.find(p => p.socket === socket).nickName;
        this.players.forEach(player => player.socket.emit(ERoomActions.SEND_MESSAGE, msg, nickName, Date.now().toString()));
    }
}
