/**
 * created by mrzluka on 29.01.2022
 */
import { Socket } from "socket.io-client";

export type THitRequest = {
    col: number,
    row: number,
    playerId: string,
    roomId: string,
}

export type TResponse = {
    error: string | undefined;
}

export type THitResponse = {
    status: "wait" | "hit" | "done",
    matrix: number[][],
    isXPlayer: boolean,
} & TResponse;

export type TCreateRoomResponse = {
    roomId: string,
} & TResponse;

export type TRoom = {
    matrix: number[][],
    players: string[],
    playersSocket?: Socket[],
    XPlayer: string;
    roomId: string;
    activePlayerNdx: number;
    winner: string | undefined;
};

export enum EError {
    NO_ROOM = "Room not found",
    WRONG_ACTION = "That's not your turn",
    CELL_ALL_READY_CLICKED = "Choose another cell",
    WRONG_INDEX = "Wrong col or row",
    GAME_OVER = "Game over",
    WRONG_PLAYER = "Wrong player",
    WAIT_SECOND_PLAYER = "Wait your mate",
}
